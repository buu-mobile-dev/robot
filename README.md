# Robot find Bomb
A command-line robot find bomb application with an entrypoint in `bin/`, library code
in `lib/`, and example unit test in `test/`.
