import 'dart:io';

import 'package:farmer/gobject.dart';

class GMap {
  final int width;
  final int height;
  final List<GObject> obj = [];

  GMap(this.width, this.height);

  void setObject(GObject obj) {
    this.obj.add(obj);
  }

  bool showObject(int x, int y) {
    for (GObject i in obj) {
      if (i.isOn(x, y)) {
        stdout.write(i.getSymbol());
        return true;
      }
    }
    return false;
  }

  void showMap() {
    _showTitle();
    for (var y = 0; y < height; y++) {
      for (var x = 0; x < width; x++) {
        if (!showObject(x, y)) {
          _showCell();
        }
      }
      _showNewLine();
    }
  }

  void _showTitle() {
    print("Map");
  }

  void _showCell() {
    stdout.write('-');
  }

  void _showNewLine() {
    print("");
  }

  bool inMap(int x, int y) {
    return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  bool isApple(int x, int y) {
    for (GObject i in obj) {
      if (i.isOn(x, y) && i.getSymbol() == 'A') {
        i.symbol = '-';
        return true;
      }
    }
    return false;
  }

  bool isWall(int x, int y) {
    for (GObject i in obj) {
      if (i.isOn(x, y) && i.getSymbol() == 'W') {
        return true;
      }
    }
    return false;
  }

  bool isGrape(int x, int y) {
    for (GObject i in obj) {
      if (i.isOn(x, y) && i.getSymbol() == 'G') {
        i.symbol = '-';
        return true;
      }
    }
    return false;
  }

  bool isAnanas(int x, int y) {
    for (GObject i in obj) {
      if (i.isOn(x, y) && i.getSymbol() == 'P') {
        i.symbol = '-';
        return true;
      }
    }
    return false;
  }
}
