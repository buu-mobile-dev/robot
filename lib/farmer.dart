import 'package:farmer/gmap.dart';
import 'package:farmer/gobject.dart';

class Farmer extends GObject {
  int grape = 0;
  int apple = 0;
  int ananas = 0;
  int _step = 0;
  final GMap map;

  Farmer(int x, int y, String symbol, this.map) : super(x, y, symbol);

  bool walk(String direction) {
    _step += 1;

    switch (direction) {
      case 'N':
      case 'w':
        if (walkN()) return false;
        break;
      case 'S':
      case 's':
        if (walkS()) return false;
        break;
      case 'E':
      case 'd':
        if (walkE()) return false;
        break;
      case 'W':
      case 'a':
        if (walkW()) return false;
        break;
      default:
    }

    _checkGrape();
    _checkApple();
    _checkAnanas();
    print(this);
    return apple == 3 && ananas == 3 && grape == 3;
  }

  void checkBomb() {
    if (map.isApple(x, y)) {
      print("Founded Bomb!!!! ( $x , $y )");
    }
  }

  bool walkW() {
    if (map.inMap(x - 1, y) && !_checkWall(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (map.inMap(x + 1, y) && !_checkWall(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (map.inMap(x, y + 1) && !_checkWall(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkN() {
    if (map.inMap(x, y - 1) && !_checkWall(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    return false;
  }

  void _checkGrape() {
    if (map.isGrape(x, y)) {
      grape += 1;
    }
  }

  void _checkApple() {
    if (map.isApple(x, y)) {
      apple += 1;
    }
  }

  void _checkAnanas() {
    if (map.isAnanas(x, y)) {
      ananas += 1;
    }
  }

  bool _checkWall(int x, int y) {
    return map.isWall(x, y);
  }

  @override
  String toString() {
    return "[STEP: $_step] Grape: $grape, Apple: $apple, Ananas: $ananas";
  }
}
