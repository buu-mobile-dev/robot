import 'package:farmer/glocation.dart';

class GObject implements GLocation {
  @override
  late int x;
  @override
  late int y;
  late String symbol;

  GObject(this.x, this.y, this.symbol);

  @override
  int getX() {
    return x;
  }

  @override
  int getY() {
    return y;
  }

  String getSymbol() {
    return symbol;
  }

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }

  @override
  String toString() {
    return "x = $x y = $y";
  }
}
