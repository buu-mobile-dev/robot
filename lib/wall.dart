import 'package:farmer/gobject.dart';

class Wall extends GObject {
  Wall(int x, int y) : super(x, y, 'W');
}
