import 'package:farmer/farmergame.dart';
import 'dart:io';

void main(List<String> arguments) {
  //stdin.readLineSync();
  GMap map = GMap(10, 10);
  Farmer farmer = Farmer(2, 2, 'x', map);
  map.setObject(farmer);
  map.setObject(Wall(4, 2));
  map.setObject(Wall(5, 8));
  map.setObject(Wall(6, 9));
  map.setObject(Ananas(5, 1));
  map.setObject(Ananas(9, 4));
  map.setObject(Ananas(3, 4));
  map.setObject(Grape(0, 3));
  map.setObject(Grape(3, 6));
  map.setObject(Grape(5, 7));
  map.setObject(Apple(9, 5));
  map.setObject(Apple(7, 3));
  map.setObject(Apple(4, 3));

  while (true) {
    map.showMap();
    String direction = (stdin.readLineSync() ?? 'q')[0];
    if (direction == 'q' || farmer.walk(direction)) {
      map.showMap();
      exitGame();
      break;
    }
  }
}

void exitGame() {
  print("Bye Bye!!!");
}
